<?php
require_once('animal.php');
require_once('Ape.php');
require_once('Frog.php');

$sheep = new Animal("Shaun");

echo "<h3>Release 0</h3>";
echo $sheep->name . "<br>";
echo $sheep->legs . "<br>";
echo $sheep->cold_blooded . "<br>";

echo "<h3>Release 1</h3>";
$sungokong = new Ape("kera sakti");
echo "Name = " . $sungokong->name . "<br>";
echo "Legs = " . $sungokong->legs . "<br>";
echo "Cold Blooded = " . $sungokong->cold_blooded . "<br>";
$sungokong->yell();

$kodok = new Frog("buduk");
echo "Name = " . $kodok->name . "<br>";
echo "Legs = " . $kodok->legs . "<br>";
echo "Cold Blooded = " . $kodok->cold_blooded . "<br>";
$kodok->jump();
